﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundManager : MonoBehaviour
{
    public int amountPositions = 50;

    [System.Serializable]
    public class Block
    {
        public GameObject go;
        public float timer;
        public bool stable;

        public void UnstableBlockUpdate()
        {
            timer -= Time.deltaTime;

            if (timer <= 0f)
            {
                go.SetActive(!go.activeSelf);
                timer = Random.Range(2, 5) + (Random.Range(0, 10)/10f);
            }
        }
    }

    public List<GameObject> blocksGO;
    List<Block> blocks;

    void Start()
    {
        blocks = new List<Block>();

        for (int i = 0; i < blocksGO.Count; i++)
        {
            Block b = new Block();

            b.go = blocksGO[i];
            b.stable = b.go.CompareTag("Stable");
            b.timer = Random.Range(1, 11) + (Random.Range(0, 10) / 10f);

            blocks.Add(b);
        }
    }

    void Update()
    {
        foreach (var b in blocks)
        {
            if (!b.stable)
                b.UnstableBlockUpdate();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIEndScene : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI namePlayer;
    [SerializeField] TextMeshProUGUI score;
    [SerializeField] TextMeshProUGUI lives;
    [SerializeField] TextMeshProUGUI deaths;

    void Start()
    {
        namePlayer.text = "Thanks for playing, " + GameManager.GetInstance().sqlConnection.userName + "!";

        score.text = GameManager.GetInstance().sqlConnection.userScore.ToString();
        deaths.text = GameManager.GetInstance().sqlConnection.userDeaths.ToString();
        lives.text = GameManager.GetInstance().lives.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class UIGameplayManager : MonoBehaviour
{
    [SerializeField] Image[] lives;
    [SerializeField] TextMeshProUGUI score;
    [SerializeField] TextMeshProUGUI deaths;
    [SerializeField] TextMeshProUGUI time;

    string FormatTime(float fTime)
    {
        TimeSpan t = TimeSpan.FromSeconds(fTime);

        return string.Format("{1:D2}:{2:D2}:{3:D2}", t.Hours, t.Minutes, t.Seconds, t.Milliseconds);
    }

    public void UpdateLives(int amountLives)
    {
        int maxLives = 3;
        if (amountLives > maxLives)
            amountLives = maxLives;
        else if (amountLives < 0)
            amountLives = 0;

        for (int i = 0; i < maxLives; i++)
        {
            if (lives[i].enabled)
                lives[i].enabled = false;
        }

        for (int i = 0; i < amountLives; i++)
        {
            lives[i].enabled = true;
        }
    }

    public void UpdateScore(int amountScore)
    {
        if (amountScore < 0)
            amountScore = 0;

        score.text = amountScore.ToString();
    }

    public void UpdateDeaths(int amountDeaths)
    {
        if (amountDeaths < 0)
            amountDeaths = 0;

        deaths.text = amountDeaths.ToString();
    }

    public void UpdateTime(float actualTime)
    {
        time.text = FormatTime(actualTime);
    }

    void Update()
    {
        UpdateTime(LevelManager.GetInstance().time);
    }
}

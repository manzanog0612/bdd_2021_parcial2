﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsManager : MonoBehaviour
{
    List<GameObject> coins;

    public Vector3[] positions;
    [SerializeField] GameObject coinPrefab;

    public delegate void OnCreatedACoinDelegate(Coin coin);
    public OnCreatedACoinDelegate onCreatedACoin;

    public int initialAmount = 10;
    public int amountCoins;

    void CreateCoin()
    {
        Vector3 pos = positions[Random.Range(0, positions.Length)];

        while (!CheckAvailablePosition(pos))
        {
            pos = positions[Random.Range(0, positions.Length)];
        }

        GameObject c = Instantiate(coinPrefab, pos, coinPrefab.transform.rotation, transform);
        coins.Add(c);

        onCreatedACoin?.Invoke(c.GetComponent<Coin>());

        c.GetComponent<Coin>().onDestroy += RemoveFromList;
    }

    public bool CheckAvailablePosition(Vector3 position)
    {
        foreach (GameObject coin in coins)
        {
            if (coin.transform.position == position)
                return false;
        }

        return true;
    }

    public void RemoveFromList(Coin c)
    {
        coins.Remove(c.gameObject);
        CreateCoin();       
    }

    void Start()
    {
        coins = new List<GameObject>();

        for (int i = 0; i < initialAmount; i++)
        {
            CreateCoin();
        }
    }

    private void Update()
    {
        amountCoins = coins.Count;
    }
}

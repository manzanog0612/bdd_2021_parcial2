using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMenuManager : MonoBehaviour
{
    [SerializeField] GameObject menu;
    [SerializeField] GameObject nameGetter;

    public void GoToScene(string scene)
    {
        switch (scene)
        {
            case "Game":
                ActivateNameGetterCanvas();
                break;
            case "Exit":
                Application.Quit();
                break;
            default:
                GameManager.GetInstance().GoToScene(scene);
                break;
        }
    }

    void ActivateNameGetterCanvas()
    {
        menu.SetActive(false);
        nameGetter.SetActive(true);

        nameGetter.GetComponent<UINameGetter>().active = true;
    }
}

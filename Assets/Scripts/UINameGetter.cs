﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UINameGetter : MonoBehaviour
{
    [SerializeField] InputField input;

    public bool active = false;

    void Update()
    {
        if (!active) return;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameManager.GetInstance().sqlConnection.userName = input.text;
            GameManager.GetInstance().GoToScene("Game");
        }
    }
}

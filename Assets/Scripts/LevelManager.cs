﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelManager : MonoBehaviour
{
    private static LevelManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    public static LevelManager GetInstance()
    {
        return instance;
    }

    [SerializeField] FallTrigger fallTrigger;
    [SerializeField] Player player;
    [SerializeField] CoinsManager coinsManager;
    [SerializeField] BackgroundManager backgroundManager;
    [SerializeField] UIGameplayManager uiManager;

    float yCoinsAxisOffSet = 1.1f; // cube.size/2 + coin.size/2 + distance between both

    public float time = 0f;

    public void GetInformation(out int deaths, out int score, out int lives)
    {
        score = player.GetScore();
        deaths = player.GetDeaths();
        lives = player.GetLives();
    }

    bool EndGame()
    {
        TimeSpan t = TimeSpan.FromSeconds(time);

        return t.Minutes >= 2;
    }

    void UpdateTime()
    {
        time += Time.deltaTime;

        if (EndGame())
        {
            GameManager.GetInstance().UploadData();
        }
    }

    void SetPositionsForCoins()
    {
        coinsManager.positions = new Vector3[backgroundManager.amountPositions];

        for (int i = 0; i < backgroundManager.amountPositions; i++)
        {
            Vector3 coinPos = backgroundManager.blocksGO[i].transform.position;
            coinPos.y += yCoinsAxisOffSet;

            coinsManager.positions[i] = coinPos;
        }
    }

    void AssignCoinDelegates(Coin c)
    {
        c.onYellowPickUp += player.AddScore;
        c.onRedPickUp += player.SubtractOneLive;
    }

    void ReappearPlayer()
    {
        foreach (GameObject block in backgroundManager.blocksGO)
        {
            Vector3 blockPos = block.transform.position;
            blockPos.y += yCoinsAxisOffSet;

            if (block.CompareTag("Stable") && coinsManager.CheckAvailablePosition(blockPos))
            {
                player.gameObject.transform.position = new Vector3(blockPos.x, blockPos.y + player.gameObject.transform.localScale.y/2, blockPos.z);
            }
        }
    }

    void Start()
    {
        SetPositionsForCoins();

        coinsManager.onCreatedACoin += AssignCoinDelegates;
        player.onReappear += ReappearPlayer;
        player.onLivesChanged += uiManager.UpdateLives;
        player.onDeathsChanged += uiManager.UpdateDeaths;
        player.onChangedScore += uiManager.UpdateScore;
        fallTrigger.onActivatedTrigger += player.SubtractOneLive;
        fallTrigger.onActivatedTrigger += ReappearPlayer;
    }

    void Update()
    {
        UpdateTime();
    }
}

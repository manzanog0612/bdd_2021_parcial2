using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveInfoManager : MonoBehaviour
{
    private static SaveInfoManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    public static SaveInfoManager GetInstance()
    {
        return instance;
    }

    int points = 0;
    int deaths = 0;
    int lives = 0;

    private void OnTriggerEnter(Collider other)
    {
        LevelManager.GetInstance().GetInformation(out deaths, out points, out lives);
    }

    public void GetSavedInformation(out int _deaths, out int _score, out int _lives)
    {
        _score = points;
        _deaths = deaths;
        _lives = lives;
    }
}

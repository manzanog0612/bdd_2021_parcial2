﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallTrigger : MonoBehaviour
{
    public delegate void onActivatedTriggerDelegate();
    public onActivatedTriggerDelegate onActivatedTrigger;

    void OnTriggerEnter(Collider other)
    {
        onActivatedTrigger?.Invoke();
    }
}

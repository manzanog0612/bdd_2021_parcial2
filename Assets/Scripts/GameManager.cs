﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            Debug.Log(name + " has been destroyed, there's already one Game Manager created.");
            return;
        }
        instance = this;
        Debug.Log(name + " has been created and is now the Game Manager.");
        DontDestroyOnLoad(gameObject);
    }

    public static GameManager GetInstance()
    {
        return instance;
    }

    public SqlConnect sqlConnection;
    public int lives;

    public void GoToScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void UploadData()
    {
        int score;
        int deaths;

        SaveInfoManager.GetInstance().GetSavedInformation(out deaths, out score, out lives);

        sqlConnection.userScore = score;
        sqlConnection.userDeaths = deaths;

        sqlConnection.CallRegister();

        GoToScene("FinalScene");
    }
}

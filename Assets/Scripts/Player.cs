﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public delegate void OnReappearDelegate();
    public OnReappearDelegate onReappear;

    public delegate void OnLivesChangedDelegate(int lives);
    public OnLivesChangedDelegate onLivesChanged;

    public delegate void OnDeathsChangedDelegate(int deaths);
    public OnDeathsChangedDelegate onDeathsChanged;

    public delegate void OnChangedScoreDelegate(int score);
    public OnChangedScoreDelegate onChangedScore;

    Animator anim;

    [SerializeField] LayerMask platformLayer;

    int score = 0;
    int lives = 3;
    int deaths = 0;

    [SerializeField] int speed = 15;
    [SerializeField] int jumpSpeed = 10;

    float horizontalAxis;

    private bool IsMoving()
    {
        horizontalAxis = Input.GetAxis("Horizontal");

        return (Mathf.Abs(horizontalAxis) > 0f);
    }

    public void AddScore()
    {
        score += 10;
        onChangedScore?.Invoke(score);
    }

    public void SubtractOneLive()
    {
        lives--;
        onLivesChanged?.Invoke(lives);

       if (lives == 0)
       {
            deaths++;
            onDeathsChanged?.Invoke(deaths);

            score = score / 2;
            onChangedScore?.Invoke(score);
            onReappear?.Invoke();

            lives = 3;
            onLivesChanged?.Invoke(lives);
       }
    }

    public int GetLives()
    {
        return lives;
    }

    public int GetDeaths()
    {
        return deaths;
    }

    public int GetScore()
    {
        return score;
    }

    bool CheckValidPosition(Vector3 targetPos)
    {
        float distance = 0.35f;
        if (horizontalAxis > 0f) //going to the right
        {
            return !Physics.Raycast(transform.position, Vector3.right, distance, platformLayer);
        }
        else
        {
            return !Physics.Raycast(transform.position, Vector3.left, distance, platformLayer);
        }
    }

    void SetWalkingAnimation(bool validPosition)
    {
        if (Mathf.Abs(horizontalAxis) > 0f && validPosition)
            anim.SetInteger("Walk", 1);
        else
            anim.SetInteger("Walk", 0);
    }

    void SetRotation(float horizontalAxis)
    {
        if (horizontalAxis > 0.0f)
        {
            transform.eulerAngles = new Vector3(0, 90, 0);
        }
        else
        {
            transform.eulerAngles = new Vector3(0, -90, 0);
        }
    }

    void SetMovement()
    {
        Vector3 targetPos = transform.position + Vector3.right * horizontalAxis * speed * Time.deltaTime;

        if (CheckValidPosition(targetPos))
        { 
            transform.position = targetPos; 
        }

        SetRotation(horizontalAxis);

        SetWalkingAnimation(CheckValidPosition(targetPos));
    }

    void SetJump()
    {
        anim.SetBool("Jump", true);
        GetComponent<Rigidbody>().AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
    }

    void Start()
    {
        onReappear?.Invoke();
        anim = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        if (IsMoving())
            SetMovement();
        else
            anim.SetInteger("Walk", 0);

        if (Input.GetKeyDown(KeyCode.Space) && Mathf.Abs(GetComponent<Rigidbody>().velocity.y) < 0.1f)
            SetJump(); 
        else
            anim.SetBool("Jump", false);
    }
}

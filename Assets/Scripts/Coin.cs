﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    MeshRenderer mr;

    bool isYellow = true;
    float timer = 1f;

    public delegate void OnPickUpDelegate();
    public OnPickUpDelegate onYellowPickUp;
    public OnPickUpDelegate onRedPickUp;

    public delegate void OnDestroyDelegate(Coin c);
    public OnDestroyDelegate onDestroy;

    void Start()
    {
        mr = GetComponentInChildren<MeshRenderer>();
        mr.material.color = Color.yellow;
    }

    void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= 0f)
        {
            isYellow = !isYellow;
            timer = 1f;

            mr.material.color = isYellow ? Color.yellow : Color.red;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (isYellow)
            onYellowPickUp?.Invoke();
        else
            onRedPickUp?.Invoke();

        onDestroy?.Invoke(this);

        Destroy(gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class SqlConnect : MonoBehaviour
{
    public string userName;
    public int userScore;
    public int userDeaths;

    public void CallRegister()
    {
        StartCoroutine(Register());
    }

    IEnumerator Register()
    {
        string sameNameError = "register.php\r\n<html>\r\n<head>\r\n\t<meta charset=\"utf-8\">\r\n\t<title>Nuestro primer ejemplo PHP</title>\r\n</head>\r\n<body>\r\n\r\n3";
        WWWForm form = new WWWForm();
        form.AddField("username", userName);
        form.AddField("score", userScore.ToString());
        form.AddField("deaths", userDeaths.ToString());
        UnityWebRequest www = UnityWebRequest.Post("http://localhost/parcial_2/register.php", form);
        yield return www.SendWebRequest();
         if (www.result == UnityWebRequest.Result.Success)
        {
            if (www.downloadHandler.text == sameNameError)
            {
                www = UnityWebRequest.Post("http://localhost/parcial_2/update.php", form);
                yield return www.SendWebRequest();

                if (www.result == UnityWebRequest.Result.Success)
                {
                    Debug.Log("Nombre updateado");
                }
            }
            else
            {
                Debug.Log("Cargado");
            }
        }
    }
}
